extends Node

func shook(shake_amt: float) -> Vector2:
	var x_amt = ceil((shake_amt * 2 * randf()) - shake_amt)
	var y_amt = ceil((shake_amt * 2 * randf()) - shake_amt)
	return Vector2(x_amt, y_amt)

func rand_in_rect(rect: Rect2) -> Vector2:
	return Vector2(randf(), randf()) * rect.size + rect.position

# same as range(from, to, step) but reversed
func rev_range_from(from: int, to: int, step: int = 1) -> Array:
	return range(to-1, from-1, step*-1)

# same range as range(x) but reversed
func rev_range(length: int) -> Array:
	return range(length-1, -1, -1)

# Use this to efficiently (hopefully) skip logging any message you already logged
# Just throw a Util.one_shot_log("Got it") in your _process method!
# Dynamic messages can also be useful, only the first instance of each variation will be logged
var _one_shot_logs: = {0: [""]}
var _one_shot_alloc: = 0
func one_shot_log(msg: String) -> void:
	var l: = len(msg)
	if l > _one_shot_alloc:
		for i in range(_one_shot_alloc+1, l+1):
			_one_shot_logs[i] = []
		_one_shot_alloc = l
	if msg not in _one_shot_logs[l]:
		print(msg)
		_one_shot_logs[l].append(msg)
# In case you want to reset a message to one shot it again
func un_cache_log_msg(msg: String) -> void:
	var l: = len(msg)
	if l in _one_shot_logs:
		_one_shot_logs[l].erase(msg)

func set_anim_player_time_on(timescale: float, on_node: Node) -> void:
	for child in on_node.get_children():
		if child is AnimationPlayer:
			child.speed_scale = timescale
		if child.get_child_count() > 0:
			set_anim_player_time_on(timescale, child)
