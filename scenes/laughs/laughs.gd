extends Node2D

const TOTAL: = 4

func _ready():
	set_amount(2)

func set_amount(new_amount: int) -> void:
	
	for i in TOTAL:
		var vis: = i < new_amount
		if vis and not $Laugh.get_child(i).visible:
			$Laugh.get_child(i).get_node("AnimationPlayer").play("emphasis")
		$Laugh.get_child(i).visible = vis
		$Unimpressed.get_child(i).visible = not vis
	
	if new_amount == -1:
		$GameOver.visible = true
