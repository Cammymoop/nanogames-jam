extends Sprite2D

@export var TIME: = 0.3
var t_left: = 0.0

func _ready():
	t_left = randf() * TIME

func _process(delta):
	t_left -= delta
	
	if t_left < 0:
		t_left += TIME
		random_frame()


func random_frame() -> void:
	var total: = hframes
	
	var next: = randi_range(0, total - 1)
	if next == frame:
		frame = (frame + ceili(total/2.0)) % total
	else:
		frame = next
	
	scale.x = sign(randf() - 0.5)
