extends Area2D

@export var spurkle: = preload("res://scenes/common/spurkle.tscn")

@export var delay_min: = 0.05
@export var delay_max: = 0.2
@export var multiple: = 3

var wait_for: = 0.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	wait_for -= delta
	if wait_for <= 0:
		wait_for = delay_min + (randf() * (delay_max - delay_min))
		
		for _i in multiple:
			var sp: = spurkle.instantiate() as Node2D
			get_parent().add_child(sp)
			sp.global_position = to_global(get_random_pos_in())

func get_random_pos_in() -> Vector2:
	var c_pos = $CollisionShape2D.position
	var the_size: = ($CollisionShape2D.shape as RectangleShape2D).size
	return Util.rand_in_rect(Rect2(c_pos - (the_size/2.0), the_size))
