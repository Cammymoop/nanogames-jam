extends Node2D

signal transition_point
signal transition_cleared

var shapes: = [
	preload("res://scenes/common/clips/blob.png"),
	preload("res://scenes/common/clips/bubbles.png"),
	preload("res://scenes/common/clips/eye.png"),
	preload("res://scenes/common/clips/man.png"),
	preload("res://scenes/common/clips/manta.png"),
	preload("res://scenes/common/clips/scrambles.png"),
	preload("res://scenes/common/clips/sqr.png"),
	preload("res://scenes/common/clips/tri.png"),
	preload("res://scenes/common/clips/upside.png"),
	
	preload("res://scenes/common/clips/sqr.png"),
]

func transition_now() -> void:
	transition_point.emit()

func transition_is_cleared() -> void:
	transition_cleared.emit()

func pick_shape() -> void:
	var shape_tex: Texture = shapes.pick_random()
	
	$ClipGroup/Clipper.texture = shape_tex
	$Clipper2/Clipper2Spr.texture = shape_tex
	
	if randf() >= 0.9:
		var cls: = $Clipper2/Clipper2Spr as Sprite2D
		cls.scale.y = -cls.scale.y
