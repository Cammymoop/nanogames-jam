extends Marker2D

@export_exp_easing() var temperature_ease: = 1.0

@export var shake_max: = 6.0
@export_exp_easing() var shake_onset: = 1.0

const SHAKE_THRESHOLD = 0.8

@onready var min_angle = rotation
@onready var max_angle = PI - rotation

@onready var origin = position

var current_amount: = 0.0

var diff: = 0.0
var offs: = 0.0

func _ready():
	offs = min_angle
	diff = max_angle - min_angle

func set_temperature(amt: float) -> void:
	current_amount = ease(amt, temperature_ease)

func _process(_delta):
	rotation = offs + (diff * current_amount)
	
	position = origin
	var shake_amt = ease(current_amount, shake_onset) * shake_max
	if shake_amt > SHAKE_THRESHOLD:
		position = origin + Util.shook(shake_amt)
	
