@tool
extends Node2D

@export var texture: Texture = preload("res://scenes/common/input_img/up.png")
@onready var sp2: Sprite2D = $Sprite2D2

var frames: = 0

func _ready():
	if Engine.is_editor_hint():
		return
	sp2.texture = texture
	
	start_anim()
	
func start_anim() -> void:
	if frames/4 <= get_index() % 4:
		return
	
	set_process(false)
	var anim: = $AnimationPlayer as AnimationPlayer
	anim.play("dance")
	
	# This shit is so broke, looping animation players don't seems to be able to keep
	# their relative time offsets in the current animation at all for some reason
	# just leaving this as is and moving on


func _process(_delta):
	if Engine.is_editor_hint():
		if texture and sp2.texture != texture:
			sp2.texture = texture
		return
	
	frames += 1
	start_anim()
