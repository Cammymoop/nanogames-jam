extends Node2D

func show_inputs(vert: bool, horiz: bool, button: bool) -> void:
	$Scale/Box/ButtonBox.visible = button
	
	$Scale/Box/DirControls.visible = false
	$Scale/Box/UDControls.visible = false
	$Scale/Box/LRControls.visible = false
	if vert and horiz:
		$Scale/Box/DirControls.visible = true
	elif vert:
		$Scale/Box/UDControls.visible = true
	elif horiz:
		$Scale/Box/LRControls.visible = true

func show_goal(texture: Texture) -> void:
	$GoalSpot/GoalImg.texture = texture

func appear() -> void:
	$AnimationPlayer.play("appear")
