class_name MinigameController
extends Node

signal ended
signal won
signal failed

signal winning

signal pause_timer
signal remove_timer

signal started

# times based on SHORT
# SHORT is 6 seconds
enum MgLength {
	SHORT, LESS_SHORT, FORGIVING, VERY_SHORT, POSTHASTE, LONG, NO_LIMIT
}
const length_multiplier: = {
	MgLength.POSTHASTE: 0.4,
	MgLength.VERY_SHORT: 0.666,
	MgLength.SHORT: 1.0,
	MgLength.LESS_SHORT: 1.4,
	MgLength.FORGIVING: 2.2,
	MgLength.LONG: 3.0
}

@export var timer_length: MgLength = MgLength.SHORT

var meta_info: = {}

var successful: = false

var time_limit: = 6.0
var has_started: = false
var stopped_timing: = false
var elapsed: = 0.0
var difficulty: = 1

var speed_scale: = 1

func _ready():
	if timer_length != MgLength.NO_LIMIT:
		time_limit *= length_multiplier[timer_length]

func _process(delta):
	if has_started and not stopped_timing:
		elapsed += speed_scale * delta
		if elapsed > time_limit:
			fail_immediate()

func set_speed_to(new_speed: float) -> void:
	speed_scale = new_speed
	# update all animation player speed scales
	Util.set_anim_player_time_on(speed_scale, get_parent())

func get_timer_amt() -> float:
	return clampf(elapsed / time_limit, 0, 1)

func cease_timing() -> void:
	stopped_timing = true
	pause_timer.emit()

func clear_timer() -> void:
	stopped_timing = true
	remove_timer.emit()

func start() -> void:
	has_started = true
	started.emit()

func _successful() -> void:
	successful = true
	winning.emit()

func win_state(leave_after: float) -> void:
	_successful()
	clear_timer()
	
	var t: = get_tree().create_timer(leave_after)
	t.timeout.connect(leave)

func win_immediate() -> void:
	_successful()
	clear_timer()
	leave()


func fail_state(leave_after: float) -> void:
	successful = false
	cease_timing()
	
	var t: = get_tree().create_timer(leave_after)
	t.timeout.connect(leave)

func fail_immediate() -> void:
	successful = false
	clear_timer()
	leave()

func leave() -> void:
	if successful:
		won.emit()
	else:
		failed.emit()
	ended.emit()
