extends Node

var all_minigames: = [
	preload("res://scenes/minigames/firstone.tscn"),
	preload("res://scenes/minigames/pizza.tscn"),
	preload("res://scenes/minigames/clothespin.tscn"),
	preload("res://scenes/minigames/chop.tscn"),
	preload("res://scenes/minigames/shapes.tscn"),
]

var minigame_meta: = [
	{v_controls= true, btn_control= true, 
	 goal_img= preload("res://scenes/minigames/firstone/fo_goal.png")},
	{v_controls= true, btn_control= false, 
	 goal_img= preload("res://scenes/minigames/pizza/pizza_goal.png")},
	{v_controls= true, h_controls= true, 
	 goal_img= preload("res://scenes/minigames/clothespin/clothespin_goal.png")},
	{btn_control= true, 
	 goal_img= preload("res://scenes/chop/chop_goal.png")},
	{btn_control= true, 
	 goal_img= "blu", pick_shape= 0},
]

var shape_goals: = [
	preload("res://scenes/minigames/shapes/cone_goal.png"),
	preload("res://scenes/minigames/shapes/cube_goal.png"),
	preload("res://scenes/minigames/shapes/donut_goal.png"),
	preload("res://scenes/minigames/shapes/lizard_goal.png"),
]

var counter: = 4

var randomized: = true
var rand_bag: = []

func get_minigame() -> Array:
	var shape: = randi_range(0, 3)
	minigame_meta[4]["pick_shape"] = shape
	minigame_meta[4]["goal_img"] = shape_goals[shape]
	#return all_minigames.pick_random()
	return rand_one() if randomized else sequential()

func rebag() -> void:
	rand_bag = range(len(all_minigames))
	rand_bag.shuffle()

func rand_one() -> Array:
	if len(rand_bag) < 1:
		rebag()
	
	var index: int = rand_bag.pop_back()
	return [all_minigames[index], minigame_meta[index]]
	
func sequential() -> Array:
	var ret: = [all_minigames[counter], minigame_meta[counter]]
	counter += 1
	counter = posmod(counter, len(all_minigames))
	return ret
