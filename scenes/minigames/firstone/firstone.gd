extends Node2D

@export var top: = 0
@export var bottom: = 155

@export var good_top: = 73
@export var good_bottom: = 115

@export var move_amt: = 360.0

@onready var controller: MinigameController = $Minigame

@onready var hammer_anim = $ControlMe/Hammer/AnimationPlayer
@onready var dog_anim = $Node2D/AnimationPlayer

var controllable: = true

func _ready():
	$ControlMe.position = Vector2.ZERO
	$ControlMe.position.y = 20


func _process(delta):
	if controller.speed_scale <= 0:
		return
	
	delta = delta * controller.speed_scale
	if not controllable:
		return
	
	var contr = Input.get_axis("dir_up", "dir_down")
	
	var move_it: Node2D = $ControlMe as Node2D
	move_it.position.y += move_amt * delta * contr
	move_it.position.y = clampf(move_it.position.y, top, bottom)
	
	if Input.is_action_just_pressed("button"):
		go()

func is_good() -> bool:
	var y = $ControlMe.position.y
	return y >= good_top and y <= good_bottom

func go() -> void:
	controllable = false
	hammer_anim.play("hammer")

func fail() -> void:
	controllable = false
	controller.fail_immediate()


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "hammer":
		var ok: = is_good()
		hammer_anim.play("done" if ok else "discard")
		if ok:
			dog_anim.play("win")
			controller.win_state(2.2)
	elif anim_name == "discard":
		fail()


func _on_minigame_failed():
	controllable = false
