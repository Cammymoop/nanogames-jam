extends Node2D

@onready var controller: MinigameController = $Minigame

func done() -> void:
	pass


func _on_dude_won():
	controller.win_state(2.5)
	await get_tree().create_timer(0.3 / controller.speed_scale).timeout
	
	$Victory/AnimationPlayer.play("pre_equip")
