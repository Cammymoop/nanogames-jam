extends Sprite2D

signal won

@export var interp_speed: = 1440.0
@export var snap_dist: = 7.0

var target_position: Vector2

@onready var tile_map: TileMap = $"../TileMap"
@onready var controller: MinigameController = $"../../Minigame"


const SKUNK = 2
const PIN = 3

const TILE_SIZE = Vector2(32, 32)

const REPEAT = 0.15
var input_repeater: = 0.0

var no_control: = false

var is_snapped: = true

func _ready():
	target_position = global_position
	
	await get_tree().process_frame
	$Timer.start(0.2 / controller.speed_scale)

func _process(delta):
	delta = delta * controller.speed_scale
	var inp_dir: = Input.get_vector("dir_left", "dir_right", "dir_up", "dir_down")
	var single_input: = single_direction(inp_dir)
	
	if no_control:
		single_input = Vector2.ZERO
	
	var first_m: = true
	
	if input_repeater > 0:
		first_m = false
		if single_input == Vector2.ZERO:
			input_repeater = 0
			return
		input_repeater -= delta
		
		if input_repeater > 0:
			single_input = Vector2.ZERO
	
	if single_input != Vector2.ZERO:
		input_repeater = REPEAT
		if first_m:
			input_repeater *= 1.5
		do_move(single_input)
	
	var interp: = (target_position - global_position).limit_length(interp_speed * delta)
	global_position += interp
	
	if not is_snapped and (target_position - global_position).length() < snap_dist:
		close_to_target()

func do_move(vec: Vector2) -> void:
	var old_pos: = target_position
	target_position = to_global(to_local(target_position) + vec * TILE_SIZE)
	
	if tile_at_target_pos() == SKUNK:
		if abs(vec.x) > 0:
			$AnimationPlayer.play("stretch")
		else:
			$AnimationPlayer.play("stretch_2")
		target_position = old_pos
		return
	elif tile_at_target_pos() == PIN:
		no_control = true
	
	is_snapped = false
	if $Timer.wait_time > 0.06:
		random_frame()
		$Timer.start(0.06)

func close_to_target() -> void:
	is_snapped = true
	global_position = target_position
	$Timer.start(0.2 / controller.speed_scale)
	
	if tile_at_target_pos() == PIN:
		won.emit()
		$Timer.stop()
		frame = 6
		tile_map.erase_cell(1, target_tile_pos())
		$"../../Scale2/SpurkleSpawner".visible = false
		set_process(false)

func single_direction(vec: Vector2) -> Vector2:
	if vec.length() < 0.1:
		return Vector2.ZERO
	
	if abs(vec.y) > abs(vec.x):
		vec.x = 0
	else:
		vec.y = 0
	return vec.normalized()

func _on_timer_timeout():
	random_frame()

func target_tile_pos() -> Vector2i:
	return tile_map.local_to_map(tile_map.to_local(target_position)) as Vector2i

func tile_at_target_pos() -> int:
	return tile_map.get_cell_source_id(1, target_tile_pos())

func random_frame() -> void:
	var total: = hframes
	
	var next: = randi_range(0, total - 1)
	if next == frame:
		frame = (frame + ceili(total/2.0)) % total
	else:
		frame = next


func _on_minigame_failed():
	no_control = true
	set_process(false)
