extends Node2D

@onready var controller: MinigameController = $Minigame

var going: = false
var arrow_target: Transform2D = Transform2D()
var from_xform: Transform2D = Transform2D()

var goal_shape_index: int = -2

var index_bag: = [0, 1, 2, 3]
var index_index: = -1

var wait_between_shapes: = 0.7
var lerp_time: = 0.22

var lerping_timer: = 0.0

var belay_that: = false

var scary_ico: = preload("res://scenes/minigames/shapes/scary_ico.tscn")

func _ready():
	index_bag.shuffle()
	
	await get_tree().process_frame
	if not controller.meta_info.has("pick_shape"):
		goal_shape_index = 2
	else:
		goal_shape_index = controller.meta_info["pick_shape"]
	
	await controller.started
	await get_tree().create_timer(0.2 / controller.speed_scale).timeout
	if belay_that:
		return
	goto_next_shape()
	
	await get_tree().create_timer(wait_between_shapes / controller.speed_scale).timeout
	if belay_that:
		return
	goto_next_shape()
	
	await get_tree().create_timer(wait_between_shapes / controller.speed_scale).timeout
	if belay_that:
		return
	goto_next_shape()
	
	await get_tree().create_timer(wait_between_shapes / controller.speed_scale).timeout
	if belay_that:
		return
	goto_next_shape()
	
	await get_tree().create_timer(wait_between_shapes / controller.speed_scale).timeout
	if belay_that:
		return
	from_xform = $Arrow.tranform
	arrow_target = $OutTarget.transform
	index_index = -1
	lerping_timer = lerp_time
	going = true

func goto_next_shape() -> void:
	index_index += 1
	arrow_target = $PointerPos.get_child(index_bag[index_index]).transform
	from_xform = $Arrow.transform
	lerping_timer = lerp_time
	going = true

func _process(delta):
	if Input.is_action_just_pressed("button"):
		belay_that = true
		going = false
		$Arrow.visible = false
		var failure: = true
		
		if index_index >= 0 and index_bag[index_index] == goal_shape_index:
			failure = false
		
		if failure:
			do_a_fail()
		else:
			do_a_success()
		return
	
	if not going:
		return
	
	if lerping_timer > 0:
		lerping_timer -= delta * controller.speed_scale
		lerping_timer = max(0, lerping_timer)
		
		var factor: = 1.0 - (lerping_timer / lerp_time)
		$Arrow.position = lerp(from_xform.origin, arrow_target.origin, factor)
		$Arrow.rotation = lerp_angle(from_xform.get_rotation(), arrow_target.get_rotation(), factor)

func do_a_fail() -> void:
	controller.fail_state(5.0)
	for t in $Targets.get_children():
		if t.get_index() == goal_shape_index:
			t.texture = preload("res://scenes/minigames/shapes/heart.png")
		else:
			var scary = scary_ico.instantiate()
			scary.position = t.position
			scary.myself = scary_ico
			scary.timescale = controller.speed_scale
			add_child(scary)

func do_a_success() -> void:
	controller.win_state(1.3)
	for t in $Targets.get_children():
		if t.get_index() == goal_shape_index:
			t.texture = preload("res://scenes/minigames/shapes/heart.png")
		else:
			t.scale *= 0.6
