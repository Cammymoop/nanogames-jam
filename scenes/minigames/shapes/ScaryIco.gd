extends Sprite2D

var distance: = 30
var timescale: = 1.0
var myself: PackedScene = null

func _ready():
	var vec = Vector2.RIGHT.rotated(PI * 2 * randf()) * distance
	await get_tree().create_timer(0.1 / timescale).timeout
	
	var next = myself.instantiate()
	next.position = position + vec
	next.myself = myself
	next.timescale = timescale
	get_parent().add_child(next)
