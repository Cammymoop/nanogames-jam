extends Node2D

@onready var controller: MinigameController = $Minigame

@onready var slicer: CharacterBody2D = $PIIIIZZAAA/Slicer
@onready var control_me: CharacterBody2D = $ControlMe

const MIN = 0.4

const AMRGIN = 1.1

var slicer_oomph: = 406.0
var obstacle_movement: = 532.0

var finish_line: = false

var controllable: = true

func _physics_process(delta):
	if finish_line:
		return
	delta = delta * controller.speed_scale
	
	var slicer_move = Vector2.LEFT * slicer_oomph * delta
	var collidion: = slicer.move_and_collide(slicer_move, true, 0.01, true)
	if collidion:
		var travel = collidion.get_travel()
		if travel.length() > MIN and travel.x < 0:
			slicer_move = Vector2.LEFT * travel
		else:
			slicer_move = Vector2.ZERO
	
	if slicer_move != Vector2.ZERO:
		slicer.move_and_collide(slicer_move, false, 0.01)
	
	if slicer.position.x <= 0:
		slicer.position = Vector2.ZERO
		finish_line = true
		controller.win_state(1.4)
	
	if not controllable:
		return
	
	var contr: = Input.get_axis("dir_up", "dir_down")
	
	if abs(contr) > 0:
		var obstacle_move = Vector2.DOWN * contr * obstacle_movement * delta
		
		var my_coll: = control_me.move_and_collide(obstacle_move, true, 0.01, true)
		if not my_coll:
			control_me.move_and_collide(obstacle_move, false, 0.01)
		else:
			var travel = my_coll.get_travel()
			control_me.move_and_collide(sign(contr) * Vector2.DOWN * travel, false, 0.01)
			
		control_me.position.x = 0


func _on_minigame_failed():
	set_physics_process(false)
