extends Node2D

@onready var controller: MinigameController = $Minigame

var chop_is_down: = false

var chop_count: = 0
var total_to_chop: = 15

var chop_texs: = [
	preload("res://scenes/chop/cucumber_chop1.png"),
	preload("res://scenes/chop/cucumber_chop2.png"),
	preload("res://scenes/chop/cucumber_chop3.png"),
	preload("res://scenes/chop/cucumber_chop4.png"),
	preload("res://scenes/chop/cucumber_chop5.png"),
	preload("res://scenes/chop/cucumber_chop6.png"),
	preload("res://scenes/chop/cucumber_chop7.png"),
	preload("res://scenes/chop/cucumber_chop8.png"),
	preload("res://scenes/chop/cucumber_chop9.png"),
	preload("res://scenes/chop/cucumber_chop10.png"),
	preload("res://scenes/chop/cucumber_chop11.png"),
	preload("res://scenes/chop/cucumber_chop12.png"),
	preload("res://scenes/chop/cucumber_chop13.png"),
	preload("res://scenes/chop/cucumber_chop14.png"),
	preload("res://scenes/chop/cucumber_chop15.png"),
	preload("res://scenes/chop/cucumber_chop16.png"),
]

var controllable: = true

func _process(_delta):
	if not controllable:
		return
	
	var pressed: = Input.is_action_pressed("button")
	if chop_is_down and not pressed:
		chop_up()
	elif not chop_is_down and pressed:
		chop_down()

func chop_down() -> void:
	chop_is_down = true
	chop_count = min(total_to_chop, chop_count + 1)
	$Scale/Knifer/Chopper.frame = 1
	$Scale/Cucumber.texture = chop_texs[chop_count]
	
	if chop_count >= total_to_chop:
		controller.win_state(1.6)

func chop_up() -> void:
	chop_is_down = false
	$Scale/Knifer/Chopper.frame = 0
	$Scale/Knifer.position.x -= 10
	
	if chop_count >= total_to_chop:
		controllable = false
		$Scale/Knifer.visible = false


func _on_minigame_failed():
	controllable = false
