extends Node2D

var clap_darker: = preload("res://scenes/encouragement/clap_darker.png")
var clap_caucasian: = preload("res://scenes/encouragement/clap_caucasian.png")
var clap_red: = preload("res://scenes/encouragement/clap_red.png")
var thumber: = preload("res://scenes/encouragement/thumbs.png")

func _ready():
	randomly_place()
	
	var clap: = $Scale/Jiggle/ClapW as Sprite2D
	if clap.position.x > 370:
		clap.scale.x = -1
	
	var clap_type = randf()
	if clap_type > 0.5:
		clap_type *= 2
		clap_type -= 1
		if clap_type < 0.5:
			clap.texture = clap_darker
		elif clap_type < 0.75:
			clap.texture = clap_caucasian
		elif clap_type < 0.92:
			clap.texture = clap_red
		else:
			clap.texture = thumber
			clap.get_node("AnimationPlayer").play("thumb_anim")
	
	var banana: = $Scale/Jiggle/Banana as Sprite2D
	if banana.position.x < 370:
		banana.scale.x = -1
	

func randomly_place():
	var positions: = $Positions.get_children()
	
	positions.shuffle()
	var j: = $Scale/Jiggle as Node2D
	for i in j.get_child_count():
		var c: = j.get_child(i) as Node2D
		if not c:
			continue
		c.global_position = positions[i].global_position
