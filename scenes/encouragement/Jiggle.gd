extends Node2D

@export var angle_max: = PI/8
@export var speed: = 0.25
@export var update_ratio: = 0.6

func _ready():
	var timer: = Timer.new()
	timer.wait_time = speed
	add_child(timer)
	timer.timeout.connect(its_time)
	timer.start()

func its_time() -> void:
	for c in get_children():
		if not c is Node2D:
			continue
		if randf() > update_ratio:
			continue
		c.rotation = rand_angle()

func rand_angle() -> float:
	return  (randf() * 2 * angle_max) - angle_max
