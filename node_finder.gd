extends Node

signal first_registered(reg_name: String, node: Node)
signal one_joined(reg_name: String, node: Node)
signal one_left(reg_name: String, node: Node)
signal unregistered(reg_name)

# Add more names here to be able to register a node/nodes to that name
enum RegNames {
	Player,
}

var only_allow_registered_names: = true
var registered_nodes: Dictionary = {}

var _complained_w_list: = false
var _signaled_name: = ""

func _ready() -> void:
	get_tree().node_removed.connect(_on_some_node_removed)

func _on_some_node_removed(the_node: Node) -> void:
	if not the_node.is_in_group("__REGISTERED"):
		return
	for reg_name in registered_nodes:
		for reg_node in registered_nodes[reg_name]:
			if the_node == reg_node:
				_unregister(reg_name, the_node)


func get_first(reg_name: String) -> Node:
	if _invalid_name_complain(reg_name, "get_first"):
		return null
	
	var nodes = _get_nodes_for_reg_name(reg_name)
	if len(nodes < 1):
		return null
	return nodes[0] as Node


func register(node: Node, as_name: String = "") -> void:
	if not (node as Node):
		return
	var n_name: = as_name
	if not as_name:
		n_name = node.get_name()
	
	if _invalid_name_complain(n_name, "register"):
		return
	
	node.add_to_group("__REGISTERED")
	
	if not registered_nodes.has(n_name):
		registered_nodes[n_name] = []
		registered_nodes[n_name].append(node)
		_signaled_name = n_name
		first_registered.emit(n_name, node)
	else:
		registered_nodes[n_name].append(node)


func _unregister(reg_name: String, the_node: Node) -> void:
	registered_nodes[reg_name].remove(the_node)
	_signaled_name = reg_name
	one_left.emit(reg_name, the_node)


# Only call this from signal callback or _signaled_name wont be set properly
# Instead of this you can modify this script to pass values from the RegNames enum rather than strings
func name_check(reg_name: String) -> bool:
	if _invalid_name_complain(reg_name, "name_check"):
		return false
	return _signaled_name == reg_name

func _get_nodes_for_reg_name(reg_name: String) -> Array:
	if reg_name not in registered_nodes:
		return []
	else:
		_clean(reg_name) # Sanity check...
		return registered_nodes[reg_name]


# Sanity check, should be removed if I can garuntee catching leaving nodes,
# not catching a node removal would likely already break something
func _clean(reg_name: String) -> void:
	if not registered_nodes.has(reg_name) or len(registered_nodes[reg_name] < 1):
		return
	
	var list: Array = registered_nodes[reg_name]
	
	for i in Util.rev_range(len(registered_nodes[reg_name])):
		var n: Node = list[i] as Node
		# Can an invalid instance ref be checked against another?
		# If so this could emit one_left for those if they werent detected.
		# I'm hoping this function is reduntant anyway.
		if not is_instance_valid(n) or not n:
			print_debug("NodeFinder: Some node left and wasn't caught!! %s" % reg_name)
			list.erase(i)
	
	if len(list) < 1:
		_signaled_name = reg_name
		unregistered.emit(reg_name)


func _invalid_name_complain(reg_name: String, action: String) -> bool:
	if not only_allow_registered_names:
		return false
	if reg_name and reg_name in RegNames:
		return false
	
	print("NodeFinder: Tried to % with name of %," % [action, reg_name])
	if not _complained_w_list:
		_complained_w_list = true
		print("Allowed names to register:")
		for rn in RegNames:
			print("  %" % rn)
	return true
