extends Node2D

var cur_minigame: MinigameController = null
var cur_minigame_from: PackedScene = null
var cur_minigame_meta: Dictionary = {}

var transition_to: = "minigame"

@onready var minigame_fetcher = $MinigameFetcher

@onready var needle = $CanvasLayer/TempMeter/Needle

@onready var transitioner = $CanvasLayer/Transitioner
@onready var transition_anim: AnimationPlayer = $CanvasLayer/Transitioner/AnimationPlayer

@onready var temp_anim: AnimationPlayer = $CanvasLayer/TempMeter/AnimationPlayer

@onready var inputs_used = $InputsUsed


var encouragement_scn: = preload("res://scenes/encouragement/encouragement.tscn")

var speedup_interval: = 8
var speedup_by: = 0.8
var max_speedups: = 2

var mg_counter: = 0
var laugh_time: = 1.6
var temp_showing: = false
var laughs: = 2

func _ready():
	randomize()
	
	await get_tree().create_timer(1.0).timeout
	que_minigame()

func activate_transition() -> void:
	if transition_to == "minigame":
		$BG/Laughs.visible = false
		inputs_used.visible = false
		create_minigame()
	elif transition_to == "in_between":
		$BG/Laughs.visible = true
		mg_counter += 1
		
		if cur_minigame.successful:
			laughs = min(4, laughs + 1)
		else:
			laughs -= 1
		
		drop_minigame()

func on_transition_cleared() -> void:
	if transition_to == "minigame":
		start_minigame()
	elif transition_to == "in_between":
		await get_tree().create_timer(0.2).timeout
		$BG/Laughs.set_amount(laughs)
		
		if laughs > -1:
			await get_tree().create_timer(laugh_time).timeout
			que_minigame()

func que_minigame() -> void:
	pick_minigame()
	show_controls_for_minigame()
	
	var t: = get_tree().create_timer(3.0)
	t.timeout.connect(into_minigame)

func show_controls_for_minigame() -> void:
	var V: bool = cur_minigame_meta.get("v_controls", false)
	var H: bool = cur_minigame_meta.get("h_controls", false)
	var btn: bool = cur_minigame_meta.get("btn_control", false)
	inputs_used.show_inputs(V, H, btn)
	inputs_used.show_goal(cur_minigame_meta["goal_img"])
	inputs_used.appear()


func into_minigame() -> void:
	transition_to = "minigame"
	transition_anim.play("shape_zoom")

func create_minigame() -> void:
	if not is_instance_valid(cur_minigame_from):
		print_debug("No minigame loaded")
		return
	
	var minigame_instance = cur_minigame_from.instantiate()
	$MinigameHere.add_child(minigame_instance)
	
	cur_minigame = minigame_instance.get_node("Minigame") as MinigameController
	if not cur_minigame:
		print_debug("No minigame controller %s" % cur_minigame_from.resource_path)
		return
	
	cur_minigame.meta_info = cur_minigame_meta
	cur_minigame.set_speed_to(cur_speed_factor())
	
	cur_minigame.ended.connect(leave_minigame)
	cur_minigame.remove_timer.connect(remove_temp)
	cur_minigame.winning.connect(encourage)

func cur_speed_factor() -> float:
	print(1.0 + (minf(floorf(mg_counter / speedup_interval), max_speedups)  * speedup_by))
	return 1.0 + (minf(floorf(mg_counter / speedup_interval), max_speedups)  * speedup_by)

func encourage() -> void:
	var enc_instance = encouragement_scn.instantiate()
	$EncHere.add_child(enc_instance)

func start_minigame() -> void:
	cur_minigame.start()
	
	temp_showing = true
	temp_anim.play("appear")

func pause_temp() -> void:
	pass # nothing particular needs to be done here yet

func remove_temp() -> void:
	if temp_showing:
		temp_showing = false
		print("leaving")
		temp_anim.play("leave")


func pick_minigame() -> void:
	drop_minigame()
	var result: Array = minigame_fetcher.get_minigame()
	cur_minigame_from = result[0]
	cur_minigame_meta = result[1]

func drop_minigame() -> void:
	var all_children = $MinigameHere.get_children()
	for c in all_children:
		c.queue_free()
		$MinigameHere.remove_child(c)
	cur_minigame = null
	cur_minigame_from = null
	cur_minigame_meta = {}
	
	if $EncHere.get_child_count() > 0:
		var enc_c = $EncHere.get_children()
		for c in enc_c:
			c.queue_free()
			$EncHere.remove_child(c)

func leave_minigame() -> void:
	remove_temp()
	transition_to = "in_between"
	var anim: = "shape_zoom" if cur_minigame.successful else "lose"
	transition_anim.play(anim)

func _process(_delta):
	if temp_showing and cur_minigame:
		needle.set_temperature(cur_minigame.get_timer_amt())
	
	if Input.is_action_just_pressed("restart"):
		get_tree().reload_current_scene()
	
	if Input.is_action_just_pressed("menu"):
		get_tree().change_scene_to_file("res://scenes/main_menu.tscn")
