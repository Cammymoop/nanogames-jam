extends Control

func _process(_delta) -> void:
	var focused: = get_viewport().gui_get_focus_owner()
	if Input.is_action_just_pressed("button"):
		if not Input.is_action_just_pressed("ui_accept") or focused == null:
			start_game()

	if Input.is_action_just_pressed("ui_down") and focused == null:
		find_child("PlayButton").grab_focus.call_deferred()

func _on_play_button_pressed():
	start_game()

func start_game() -> void:
	if OS.has_feature("web"):
		get_tree().change_scene_to_file("res://scenes/pre_cache.tscn")
	else:
		get_tree().change_scene_to_file("res://scenes/game_scene.tscn")


func _on_exit_button_pressed():
	get_tree().quit()
